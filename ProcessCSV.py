import os
import re
import datetime
import boto3
import sys
import logging
from decimal import Decimal
from hashlib import sha256

# configure the logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# set some default settings (to be used with env args)
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
DEFAULT_ENCODING = 'iso-8859-15'
DEFAULT_REGION = 'us-east-2'
DEFAULT_ENDPOINT = 'http://localhost:8000'

# initializes the S3 client
s3_client = boto3.client('s3')

# connect to dynamodb
dynamodb = boto3.resource(
    'dynamodb', region_name=DEFAULT_REGION, endpoint_url=DEFAULT_ENDPOINT)


class CSVFormatException(Exception):
    """
    A Exception to deal with the line that was not able to be processed
    """

    def __init__(self, line, tail):
        self.line = line
        self.tail = tail

    def __str__(self):
        return "Error! Check line: {}".format(self.line)

# ===
# Define some models
# Can expand to have serializers and other validators
# ===


class DynamoTable(object):
    """
    A simple DynamoDB Table composer.
    """

    __slots__ = ['table']

    def __init__(self, table):
        self.table = dynamodb.Table(table)

    def save(self, data):
        response = self.table.get_item(Key=dict(code=data.get('code')))
        item = response.get('Item', None)
        # for now there is no update
        # only creation
        # update rules must be deffined yet
        if not item:
            self.table.put_item(Item=data)


class SuperiorAgency(object):
    __slots__ = ['code', 'name', 'table']

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class SubordinatedAgency(object):
    __slots__ = ['code', 'name', 'table', ]

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class ManagementUnit(object):
    __slots__ = ['code', 'name', 'table']

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class Role(object):
    __slots__ = ['code', 'name', 'table', ]

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class SecondaryRole(object):
    __slots__ = ['code', 'name', 'table', ]

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class Project(object):
    __slots__ = ['code', 'name', 'table', ]

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class Action(object):
    __slots__ = ['code', 'name', 'table', ]

    def __init__(self, code, name="NULL"):
        self.code = code
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    def __repr__(self):
        return "<{}: code={}, name={}>".format(self.__class__.__name__,
                                               self.code,
                                               self.name)

    def save(self):
        self.table.save(dict(code=self.code, name=self.name))


class Document(object):
    __slots__ = ['code', 'management', 'date', 'value', 'table']

    def __init__(self, code, management="NULL", date="NULL", value="NULL"):
        self.code = code
        self.management = management
        self.date = date
        self.value = value
        self.table = DynamoTable(self.__class__.__name__)

    def __str__(self):
        return "{}({}): {}".format(self.code, self.date, self.value)

    def __repr__(self):
        return "<{}: code={}, date={}, value{:.2f}>"\
            .format(self.__class__.__name__,
                    self.code,
                    self.date.isoformat(),
                    self.value)

    def save(self):
        self.table.save(dict(
            code=self.code,
            management=self.management,
            date=self.date,
            value=self.value
        ))


class Grantee(object):
    __slots__ = ['cpf', 'name', 'code', 'table']

    def __init__(self, code, cpf="NULL", name="NULL"):
        self.code = code
        self.cpf = cpf
        self.name = name
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.cpf, self.name)

    def __repr__(self):
        return "<{}: cpf={}, name={}>".format(self.__class__.__name__,
                                              self.cpf,
                                              self.name)

    def save(self):
        self.table.save(dict(
            code=self.code,
            cpf=self.cpf,
            name=self.name,
        ))


class DailyExpanses(object):
    __slots__ = [
        'description',
        'superior_agency',
        'subordinated_agency',
        'management_unit',
        'role',
        'secondary_role',
        'project',
        'action',
        'document',
        'grantee',
        'code',
        'table',
    ]

    def __init__(self, **kwargs):
        m = sha256()
        m.update('*'.join(kwargs.values()).encode())
        self.code = m.hexdigest()
        self.description = kwargs.get('description', "NULL")
        self.grantee = kwargs.get('grantee', "NULL")
        self.document = kwargs.get('document', "NULL")
        self.action = kwargs.get('action', "NULL")
        self.project = kwargs.get('project', "NULL")
        self.secondary_role = kwargs.get('secondary_role', "NULL")
        self.role = kwargs.get('role', "NULL")
        self.management_unit = kwargs.get('management_unit', "NULL")
        self.subordinated_agency = kwargs.get('subordinated_agency', "NULL")
        self.superior_agency = kwargs.get('superior_agency', "NULL")
        self.table = DynamoTable(__class__.__name__)

    def __str__(self):
        return "{}: {}".format(self.code, self.description)

    def __repr__(self):
        return "<{} code={}, description={}".format(self.__class__.__name__,
                                                    self.code,
                                                    self.description)

    def save(self):
        self.table.save(dict(
            description=self.description,
            superior_agency=self.superior_agency,
            subordinated_agency=self.subordinated_agency,
            management_unit=self.management_unit,
            role=self.role,
            secondary_role=self.secondary_role,
            project=self.project,
            action=self.action,
            document=self.document,
            grantee=self.grantee,
            code=self.code,
        ))

# ===
# End models
# ===


def manage_data(row):
    """
    Given a file row (already splitted), this function gets each value
    and creates an object to be saved.
    """
    cos, nos, csub, nsub, cung, nung, cfunc, nfunc, csubfunc, nsubfunc, \
        cprog, nprog, caction, naction, lc, cpffav, nfav, docpag, gpag, \
        dtpag, vpag = row

    sup_a = SuperiorAgency(code=cos, name=nos)
    sup_a.save()

    sub_a = SubordinatedAgency(code=csub, name=nsub)
    sub_a.save()

    mu = ManagementUnit(code=cung, name=nung)
    mu.save()

    role = Role(code=cfunc, name=nfunc)
    role.save()

    s_role = SecondaryRole(code=csubfunc, name=nsubfunc)
    s_role.save()

    project = Project(code=cprog, name=nprog)
    project.save()

    action = Action(code=caction, name=naction)
    action.save()

    try:
        doc_date = datetime.datetime.strptime(dtpag, "%d/%m/%Y").isoformat()
    except ValueError:
        doc_date = 'NULL'

    doc = Document(
        code=docpag,
        date=doc_date,
        value=Decimal(str(vpag).replace(',', '.')),
        management=gpag,
    )
    doc.save()

    grantee_code = "{}{}".format(re.sub(r'\*|\.|\-', '', cpffav),
                                 nfav.replace(' ', ''))

    grantee = Grantee(code=grantee_code, name=nfav, cpf=cpffav)
    grantee.save()

    daily_expanses = DailyExpanses(**dict(
        grantee=grantee.code,
        document=doc.code,
        action=action.code,
        project=project.code,
        secondary_role=s_role.code,
        role=role.code,
        management_unit=mu.code,
        subordinated_agency=sub_a.code,
        superior_agency=sup_a.code,
        description=lc,
    ))
    daily_expanses.save()


def read_csv(file):
    """
    Givent the original CSV string. This function gets the string, splits by
    \\n and then for each row it splits by \\t.
    If there is any error while processing a row, a custom Exception is raised
    with the ERROR LINE and the rest of the file.
    The rest of the file is sent so we can skip the line and keep processing
    the other lines in a new file.
    """
    field_names = [
        'cos',      # Código Órgão Superior
        'nos',      # Nome Órgão Superior
        'csub',     # Código Órgão Subordinado
        'nsub',     # Nome Órgão Subordinado
        'cung',     # Código Unidade Gestora
        'nung',     # Nome Unidade Gestora
        'cfunc',    # Código Função
        'nfunc',    # Nome Função
        'csubfunc',  # Código Subfunção
        'nsubfunc',  # Nome Subfunção
        'cprog',    # Código Programa
        'nprog',    # Nome Programa
        'caction',  # Código Ação
        'naction',  # Nome Ação
        'lc',       # Linguagem Cidadã
        'cpffav',   # CPF Favorecido
        'nfav',     # Nome Favorecido
        'docpag',   # Documento Pagamento
        'gpag',     # Gestão Pagamento
        'dtpag',    # Data Pagamento
        'vpag',     # Valor Pagamento
    ]

    # reader = csv.DictReader(file, delimiter='\t', fieldnames=field_names)

    # skip the header
    # next(reader)

    # avoid the NULL byte error from CSV module
    rows = file.split('\n')

    for idx, row in enumerate(rows[1:]):
        # print("PROCESSING LINE: {}".format(idx))
        try:
            manage_data(row.replace('"', '').split('\t'))
        except Exception as e:
            raise CSVFormatException(
                line=idx + 1, tail='\n'.join(rows[idx + 2:]))


def handler(event, context):
    """
    The main handler.
    We set a process_time to know when the file was processed.
    We get the bucket and the key and then get the file.
    In the end, every successful file is sent to the same bucket in a folder
    called 'done'.
    If there was a problem, the file is sent to the same bucket in a folder
    called 'error'.
    """
    for record in event['Records']:
        process_time = datetime.datetime.utcnow().isoformat()

        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']

        done_path = 'done/DONE_{}_{}'.format(process_time, key)
        error_Path = 'error/ERR_{}_{}'.format(process_time, key)

        # gets the object (file) from the Bucket
        obj = s3_client.get_object(Bucket=bucket, Key=key)

        logger.info("Starting...")
        try:
            # converts the object bytes to a string
            file_data = obj['Body'].read().decode(DEFAULT_ENCODING)

            # starts reading
            read_csv(file_data)

        except CSVFormatException as e:
            logger.error(
                "ERROR READING THE FILE: {} - LINE: {}".format(e, e.line))
            dest_path = error_Path

            # skips one line and uploads the rest of the file
            s3_client.put_object(
                Body=e.tail.encode(),
                Bucket=bucket,
                Key="LINE_{}_{}".format(.line + 1, key))
        except Exception as e:
            logger.error("ERROR READING THE FILE: {}".format(e))
        else:
            dest_path = done_path
        finally:
            # copy the original file to the done / error folder
            s3_client.copy_object(CopySource=dict(
                Bucket=bucket, Key=key), Bucket=bucket, Key=dest_path)
            # deletes the original file from the root
            s3_client.delete_object(Bucket=bucket, Key=key)

        logger.info("Starting... DONE!")
