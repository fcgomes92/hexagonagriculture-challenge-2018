#!/bin/bash
cd .. \
& rm ./ProcessCSV.zip \
&& zip -g ProcessCSV.zip ProcessCSV.py \
&& zip -g ProcessCSV.zip .env/lib/python3.6/site-packages/*
