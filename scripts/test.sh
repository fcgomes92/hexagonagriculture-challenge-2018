#!/bin/bash
# to upload the file:
# ~/.local/bin/aws s3 cp ./data/201711_Diarias.csv s3://hexagonagriculture
scripts/build.sh
scripts/deploy.sh
~/.local/bin/aws lambda invoke \
--invocation-type Event \
--function-name ProcessCSV \
--region us-east-2 \
--payload file://$(pwd)/scripts/inputFile.txt \
./outputfile.txt
