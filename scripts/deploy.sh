#!/bin/bash
# ~/.local/bin/aws lambda create-function \
# --region us-east-2 \
# --function-name ProcessCSV \
# --zip-file fileb://$(pwd)/ProcessCSV.zip \
# --role arn:aws:iam::426487435704:role/hexagonagriculture-process-csv \
# --handler ProcessCSV.handler \
# --runtime python3.6 \
# --timeout 3600 \
# --memory-size 2048
~/.local/bin/aws lambda update-function-code \
--function-name ProcessCSV \
--zip-file fileb://$(pwd)/ProcessCSV.zip
~/.local/bin/aws lambda update-function-configuration \
--function-name ProcessCSV \
--timeout 300 \
--memory-size 3008